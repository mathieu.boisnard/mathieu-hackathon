export type DialogType = {
  message: string
  me?: boolean
  main?: boolean
  loading?: boolean
}

package com.example.hackathon

import com.google.cloud.vertexai.generativeai.ChatSession
import com.google.cloud.vertexai.generativeai.ContentMaker
import com.google.cloud.vertexai.generativeai.PartMaker
import com.google.cloud.vertexai.generativeai.ResponseHandler
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile

const val LOCATION = "europe-west1"
const val PROJECT = "malt-hackathon-2024-06"

@RestController
@RequestMapping("/api")
class AIApi(
    aiChat: AIChat,
) {
    private val chatSession: ChatSession = aiChat.initChat()

    @PostMapping("/prompt")
    fun prompt(prompt: String): ResponseEntity<MessageResponse> {
        val response = chatSession.sendMessage(prompt)
        val output = ResponseHandler.getText(response)
        return ResponseEntity.ok(MessageResponse(output, listOf()))
    }

    @PostMapping("/file")
    fun file(@RequestParam file: MultipartFile): ResponseEntity<MessageResponse> {

        val response = chatSession.sendMessage(
            ContentMaker.fromMultiModalData(
                PartMaker.fromMimeTypeAndData("audio/mp3", file.bytes),
                "Create a json with the text you understand, and use the system instruction to analyse the need and compute information, json be like {'text': 'text you understand', 'answer': 'your answer with your analysis'"
        ))

        val output = ResponseHandler.getText(response)
        return ResponseEntity.ok(MessageResponse(output, listOf()))
    }
}
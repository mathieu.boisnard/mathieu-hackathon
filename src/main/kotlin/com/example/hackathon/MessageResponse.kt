package com.example.hackathon


data class MessageResponse(
    val message: String,
    val tags: List<String>
)